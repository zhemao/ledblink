CC=avr-gcc
LD=$(CC)
OBJCOPY=avr-objcopy
CHIP_NAME=atmega328
CFLAGS=-Os -Wall -DF_CPU=8000000
LDFLAGS=
CHIP_ID=m328p
PORT=/dev/ttyACM0
BAUD_RATE=19200

ledblink.hex:

ledblink.o: ledblink.c
	$(CC) -c $(CFLAGS) -mmcu=$(CHIP_NAME) $< -o $@

%.elf: %.o
	$(LD) $(LDFLAGS) -mmcu=$(CHIP_NAME) $< -o $@ 

%.hex: %.elf
	$(OBJCOPY) -O ihex -R .eeprom $< $@

upload:
	avrdude -p $(CHIP_ID) -P $(PORT) -c avrisp -b $(BAUD_RATE) -U flash:w:ledblink.hex

clean:
	rm -f *.hex *.elf *.o *.lst *.map
